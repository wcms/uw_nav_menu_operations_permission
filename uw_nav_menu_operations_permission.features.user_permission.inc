<?php

/**
 * @file
 * uw_nav_menu_operations_permission.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_nav_menu_operations_permission_user_default_permissions() {
  $permissions = array();

  // Exported permission: menu permission alter.
  $permissions['menu permission alter'] = array(
    'name' => 'menu permission alter',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'menu_operations_permission',
  );

  return $permissions;
}
